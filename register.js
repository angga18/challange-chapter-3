const express = require("express");
const app = express();
let posts = require("./posts.json");

app.use(express.json());

//login
app.post("/api/login", (req, res) => {
  let user = posts.find(function (user) {
    return user.email === req.body.email && user.password === req.body.password;
  });

  if (!user) {
    res.status(401).json("ada yang salah antara email atau pasword");
    return;
  }

  res.status(200).json(user);
  console.log(user);
});

//cek data ada/tidak
app.get("/", (req, res) => {
  res.send(posts);
});

app.post("/api/register", (req, res) => {
  const { email, password } = req.body;
  const id = posts[posts.length - 1].id + 1;
  const post = {
    id,
    email,
    password,
  };
  if (!email) {
    res.send("email harus diisi");
  }
  if (!password) {
    res.send("pasword harus diisi");
  }

  posts.push(post);

  res.status(201).json(post);
});

app.listen(8000, () => {
  console.log("server runing on port 8000");
});
