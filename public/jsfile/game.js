class player {
  constructor(hand) {
    this.hand = hand;
  }
  getHand() {
    return this.hand;
  }
}
let pbatu = document.getElementById("player-batu");
let pkertas = document.getElementById("player-kertas");
let pgunting = document.getElementById("player-gunting");
let compbatu = document.getElementById("comrock");
let compkertas = document.getElementById("compaper");
let compgunting = document.getElementById("comscissor");
let textResult = document.getElementById("section-vs");
let btnrefresh = document.getElementById("refres");
let playerHands = document.getElementsByClassName("player-hand");
btnrefresh.addEventListener("click", function (e) {
  resetGame();
});

for (let i = 0; i < playerHands.length; i++) {
  const element = playerHands[i];
  element.addEventListener("click", function (e) {
    resetSelector();
    element.classList.add("selected");
    let playerHands = new player(element.id);
    let ComputerHand = new computer();
    let Result = new Match(playerHands, ComputerHand);
    let info = Result.getHasil();
    textResult.classList.remove("section-vs");
    if (info == "player1 win") {
      textResult.innerText = "player1 win";
      textResult.classList.add("player-win");
    }
    if (info == "draw") {
      textResult.innerText = "draw";
      textResult.classList.add("player-win");
    }
    if (info == "com win") {
      textResult.innerText = "com win";
      textResult.classList.add("player-win");
    }
  });
}

function resetPlayerHands() {
  for (let i = 0; i < playerHands.length; i++) {
    const element = playerHands[i];
    element.classList.remove("selected");
  }
}

function resetComputerHands() {
  for (let i = 0; i < playerHands.length; i++) {
    const element = playerHands[i];
    element.classList.remove("selected");
  }
}

function resetTextResult() {
  textResult.innerText = "vs";
  textResult.className = "section-vs";
}

function resetGame() {
  //resetHands(playerHands);
  //resetHands(ComputerHand);
  //resetTextResult();

  resetPlayerHands();
  resetComputerHands();
  resetTextResult();

  //location.reload(true);
}

let Handcom = "rock";
if (Handcom == "rock") {
  compbatu.classList.add("selected");
}

function resetSelector() {
  for (let i = 0; i < playerHands.length; i++) {
    const element = playerHands[i];
    element.classList.remove("selected");
  }
}

class human extends player {
  constructor(hand, name) {
    super(hand);
    this.name = name;
  }
}
class computer extends player {
  constructor() {
    let random = Math.floor(Math.random() * Math.floor(3));
    let hand = "scissor";
    if (random == 0) {
      hand = "rock";
    }
    if (random == 1) {
      hand = "paper";
    }
    super(hand);
  }
}

class Match {
  constructor(player1, com) {
    this.player1 = player1;
    this.com = com;
  }
  getHasil() {
    if (this.player1.getHand() == this.com.getHand()) {
      return "draw";
    }
    if (this.player1.getHand() == "rock") {
      if (this.com.getHand() == "paper") {
        return "com win";
      }
      if (this.com.getHand() == "scissor") {
        return "player1 win";
      }
    }
    if (this.player1.getHand() == "paper") {
      if (this.com.getHand() == "rock") {
        return "player1 win";
      }
      if (this.com.getHand() == "scissor") {
        return "com win";
      }
    }
    if (this.player1.getHand() == "scissor") {
      if (this.com.getHand() == "paper") {
        return "player1 win";
      }
      if (this.com.getHand() == "rock") {
        return "com win";
      }
    }
    console.log(
      `player1 : ${this.player1.getHand()}, com: ${this.com.getHand()}`
    );
  }
}
playerA = new human("rock", "paper");
playerB = new computer();
playerB.getHand();

match = new Match(playerA, playerB);
console.log(
  `Player1 : ${playerA.getHand()}, com: ${playerB.getHand()}, Result: ${match.getHasil()}`
);
