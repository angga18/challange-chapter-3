//import express
const express = require("express");
const app = express();
const path = require("path");
//create server
const port = 3000;
//set view engine

app.set("view engine", "ejs");

app.use(express.static("public"));
// create endpoint
app.get("/game", (req, res) => {
  res.render("game");
});
app.get("/landing", (req, res) => {
  res.render("landing");
});
app.get("/", (req, res) => {
  res.render("landing");
});

app.listen(port, () =>
  console.log(`server runing at http://localhost:${port}`)
);
