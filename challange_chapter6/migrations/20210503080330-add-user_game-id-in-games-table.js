"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("games", "user_game_id", {
      type: Sequelize.INTEGER,
      references: {
        model: "user_games",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn("games", "user_game_id");
  },
};
