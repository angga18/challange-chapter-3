"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("games", "user_game_history_id", {
      type: Sequelize.INTEGER,
      references: {
        model: "user_game_history",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn("games", "user_game_history_id");
  },
};
