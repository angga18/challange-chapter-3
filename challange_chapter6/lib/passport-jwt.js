const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { User } = require("../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "rahasia",
    },
    (payload, done) => {
      User.findByPk(payload.id)
        .then((user) => done(null, user))
        .catch((err) => done(err, false));
    }
  )
);

module.exports = passport;
