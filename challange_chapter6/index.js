const express = require("express");
const app = express();
var cookieParser = require("cookie-parser");
//---
const session = require("express-session");
const flash = require("express-flash");
const authhandler = require("./controllers/loginController");
const passport = require("./lib/passport");
const passportJwt = require("./lib/passport-jwt");
//--
const apiAuthRouter = require("./router/api/auth");
const apiRoomRouter = require("./router/api/createroom");

app.use(
  session({
    secret: "rahasia",
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passportJwt.initialize());
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
// view engine
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/challange_chapter6/public", express.static("public"));
app.set("view engine", "ejs");

const routerBiodata = require("./router/biodata");
const routerHistory = require("./router/history");
const routerUser = require("./router/user");
const routerlogin = require("./router/login");
app.use(routerBiodata);
app.use(routerHistory);
app.use(routerUser);
app.use(routerlogin);
app.use("/api/auth", apiAuthRouter);
app.use("/api/room", apiRoomRouter);

app.listen(3000, () => console.log(`Web App Up in http://localhost:3000`));
