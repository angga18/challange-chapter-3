const passportJwt = require("../lib/passport-jwt");

module.exports = passportJwt.authenticate("jwt", {
  session: false,
});
