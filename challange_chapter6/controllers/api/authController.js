const { User } = require("../../models");

const registerAction = function (req, res, next) {
  User.register(req.body)
    .then((User) => {
      res.json({
        id: User.id,
        username: User.username,
      });
    })
    .catch((err) => next(err));
};

const loginAction = function (req, res, next) {
  User.authenticate(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
        token: user.generateToken(),
      });
    })
    .catch((err) => next(err));
};

const profileAction = function (req, res, next) {
  res.json(req.user);
};

module.exports = { registerAction, loginAction, profileAction };
