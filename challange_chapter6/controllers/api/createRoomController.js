const { createroom } = require("../../models");

const createRoomAction = function (req, res, next) {
  createroom
    .createroom(req.body)
    .then((createroom) => {
      res.json({
        id: createroom.id,
        room_name: createroom.room_name,
      });
    })
    .catch((err) => next(err));
};
module.exports = { createRoomAction };
