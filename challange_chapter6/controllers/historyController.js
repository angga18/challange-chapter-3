const { game, user_game, user_game_history } = require("../models");

const createHistory = (req, res) => {
  res.render("history/formhistory");
};
const postHistory = (req, res) => {
  user_game_history
    .create({
      hero: req.body.hero,
      arena: req.body.arena,
      score: req.body.score,
      result: req.body.result,
      achievement: req.body.achievement,
    })
    .then(function (user_game_history) {
      res.redirect("/user_game_history/listhistory");
    });
};
const allHistory = (req, res) => {
  user_game_history.findAll().then((user_game_history) => {
    res.render("history/listhistory", { user_game_history });
  });
};
const oneHistory = (req, res) => {
  user_game_history
    .findOne({
      where: { id: req.params.id },
    })
    .then((user_game_history) => {
      res.render("history/detailhistory", { user_game_history });
    });
};
const updateHistory = (req, res) => {
  user_game_history
    .findOne({
      where: { id: req.params.id },
    })
    .then((user_game_history) => {
      if (!user_game_history) {
        res.status(404).send("Not found");
        return;
      }

      res.render("history/updatehistory", { user_game_history });
    });
};
const historypostUpdate = (req, res) => {
  user_game_history
    .update(
      {
        hero: req.body.hero,
        arena: req.body.arena,
        score: req.body.score,
        result: req.body.result,
        achievement: req.body.achievement,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (user_game_history) {
      res.redirect("/user_game_history/listhistory");
    });
};
const deleteHistory = (req, res) => {
  user_game_history
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/user_game_history/listhistory");
    });
};
// end history
module.exports = {
  createHistory,
  postHistory,
  allHistory,
  oneHistory,
  updateHistory,
  historypostUpdate,
  deleteHistory,
};
