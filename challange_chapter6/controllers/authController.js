const { User } = require("../models");
const passport = require("../lib/passport");

const registerAction = function (req, res, next) {
  User.register(req.body)
    .then(() => res.redirect("/login"))
    .catch((err) => next(err));
};

const loginAction = passport.authenticate("local", {
  successRedirect: "/account",
  failureRedirect: "/login",
  failureFlash: true,
});

const userSessionAction = (req, res, next) => {
  res.render("session", req.user.dataValues);
};

module.exports = { registerAction, loginAction, userSessionAction };
