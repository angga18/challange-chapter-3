const { game, user_game, user_game_history } = require("../models");

const biodataForm = (req, res) => {
  res.render("biodata/FormUserGameBiodata");
};

const signinPost = (req, res) => {
  game
    .create({
      nickname: req.body.nickname,
      negara: req.body.negara,
      kota: req.body.kota,
      pangkat: req.body.pangkat,
    })
    .then(function (game) {
      res.redirect("/game/list");
    });
};

// end login
// get all & one game biodata
const getGame = (req, res) => {
  game
    .findAll({
      include: user_game,
      include: user_game_history,
    })
    .then((game) => {
      res.render("biodata/list", { game });
    });
};
const getgameOne = (req, res) => {
  game
    .findOne({
      where: { id: req.params.id },
    })
    .then((game) => {
      res.render("biodata/detail", { game });
    });
};
// end get all & one biodata
// update & delete game
const gameUpdate = (req, res) => {
  game
    .findOne({
      where: { id: req.params.id },
    })
    .then((game) => {
      if (!game) {
        res.status(404).send("Not found");
        return;
      }

      res.render("biodata/update", { game });
    });
};

const postUpdate = (req, res) => {
  game
    .update(
      {
        nickname: req.body.nickname,
        negara: req.body.negara,
        kota: req.body.kota,
        level: req.body.level,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (game) {
      res.redirect("/biodata/list");
    });
};
const gameDelete = (req, res) => {
  game
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/biodata/list");
    });
};
module.exports = {
  biodataForm,
  signinPost,
  getGame,
  getgameOne,
  gameUpdate,
  postUpdate,
  gameDelete,
};
