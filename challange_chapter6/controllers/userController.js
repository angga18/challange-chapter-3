const { user_game } = require("../models");

const userForm = (req, res) => {
  res.render("userGame/formUserGame");
};

const userPost = (req, res) => {
  user_game
    .create({
      username: req.body.username,
      password: req.body.password,
    })
    .then(function (user_game) {
      res.redirect("/user_game/listUserGame");
    });
};
const userallPost = (req, res) => {
  user_game.findAll().then((user_game) => {
    res.render("userGame/listUserGame", { user_game });
  });
};
const useronePost = (req, res) => {
  user_game
    .findOne({
      where: { id: req.params.id },
    })
    .then((user_game) => {
      res.render("userGame/detailUserGame", { user_game });
    });
};
const userUpdate = (req, res) => {
  user_game
    .findOne({
      where: { id: req.params.id },
    })
    .then((user_game) => {
      if (!user_game) {
        res.status(404).send("Not found");
        return;
      }

      res.render("userGame/updateUserGame", { user_game });
    });
};
const userpostUpdate = (req, res) => {
  user_game
    .update(
      {
        username: req.body.username,
        password: req.body.password,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (user_game) {
      res.redirect("/user_game/listUserGame");
    });
};
const userDelete = (req, res) => {
  user_game
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/user_game/listUserGame");
    });
};

module.exports = {
  userForm,
  userPost,
  userallPost,
  useronePost,
  userUpdate,
  userpostUpdate,
  userDelete,
};
