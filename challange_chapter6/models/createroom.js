"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class CreateRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static CreateRoom = function ({ room_name }) {
      return this.create({
        room_name: room_name,
      });
    };
  }

  CreateRoom.init(
    {
      room_name: DataTypes.STRING,
      player1_id: DataTypes.INTEGER,
      player2_id: DataTypes.INTEGER,
      player1_hand: DataTypes.STRING,
      player2_hand: DataTypes.STRING,
      result: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "CreateRoom",
    }
  );
  return CreateRoom;
};
