"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {}

    static #encrypt = function (password) {
      return bcrypt.hashSync(password, 10);
    };
    // static #encrypt = function (password) {
    //   console.log(password);
    //   return bcrypt.hashSync(password, 10);
    // };

    static register = function ({ username, password }) {
      let encryptedPassword = this.#encrypt(password);
      return this.create({ username, password: encryptedPassword });
    };
    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payLoad = {
        id: this.id,
        username: this.username,
      };
      const secret = "rahasia";
      return jwt.sign(payLoad, secret);
    };

    static authenticate = async function ({ username, password }) {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) {
          return Promise.reject("User tidak ditemukan!");
        }
        if (!user.checkPassword(password))
          return Promise.reject("Invalid username & password");

        return Promise.resolve(user);
      } catch (error) {
        return Promise.resolve(error);
      }
    };

    // static authenticate = async function ({ username, password }){
    //   try {
    //     const user = await this.findOne({ where: { username } });
    //     if (!user) {
    //       return Promise.reject("invalid username&password");
    //     }

    //     if (!user.checkPassword(password))
    //       return Promise.reject("invalid username&password");
    //     {
    //       return Promise.resolve(user);
    //     }
    //   } catch (error) {
    //     return Promise.reject(error);
    //   }
    // };
  }
  User.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
