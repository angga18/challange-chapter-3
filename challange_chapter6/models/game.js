"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class game extends Model {
    static associate(models) {
      models.game.user_game = models.game.belongsTo(models.user_game, {
        foreignKey: "user_game_id",
      });
    }
    static associate(models) {
      models.game.user_game_history = models.game.belongsTo(
        models.user_game_history,
        {
          foreignKey: "user_game_history_id",
        }
      );
    }
  }
  game.init(
    {
      nickname: DataTypes.STRING,
      negara: DataTypes.STRING,
      kota: DataTypes.STRING,
      level: DataTypes.STRING,
      user_gameId: {
        type: DataTypes.INTEGER,
        field: "user_game_id",
      },
    },
    {
      sequelize,
      modelName: "game",
      tableName: "games",
    }
  );
  return game;
};
