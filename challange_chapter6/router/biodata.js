const router = require("express").Router();
const biodataController = require("../controllers/biodataController");

router.get("/game/create", biodataController.biodataForm);
router.post("/game", biodataController.signinPost);
router.get("/game/list", biodataController.getGame);
router.get("/game/detail/:id", biodataController.getgameOne);
router.get("/game/update/:id", biodataController.gameUpdate);
router.post("/game/update", biodataController.postUpdate);
router.get("/game/delete/:id", biodataController.gameDelete);

module.exports = router;
