const express = require("express");
const router = require("express").Router();
const authController = require("../../controllers/api/authController");
const restrictJwt = require("../../middlewares/restrict-Jwt");

router.post("/register", authController.registerAction);
router.post("/login", authController.loginAction);
router.get("/profile", restrictJwt, authController.profileAction);

module.exports = router;
