var express = require("express");
var router = express.Router();
const roomController = require("../../controllers/api/createRoomController");
const restrictJwt = require("../../middlewares/restrict-Jwt");

router.post("/create-room", restrictJwt, roomController.createRoomAction);
// router.post("/fight/:match_id", restrictJwt, matchController.fightAction);
// router.get("/detail-match/:id", restrictJwt, matchController.detailMatchAction);

module.exports = router;
