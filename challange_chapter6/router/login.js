const router = require("express").Router();
const loginController = require("../controllers/loginController");
const authController = require("../controllers/authController");
const restrict = require("../middlewares/restrict");

//home
router.get("/", loginController.home);
// router.post("/", loginController.signinHandler);

//home-index
router.get("/account", restrict, (req, res) => res.render("home-index"));
//register
router.get("/register", (req, res) => res.render("register"));
router.post("/register", authController.registerAction);
//login
router.get("/login", (req, res) => res.render("login"));
router.post("/login", authController.loginAction);

router.get("/session", restrict, authController.userSessionAction);

module.exports = router;
