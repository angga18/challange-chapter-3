const router = require("express").Router();
const userController = require("../controllers/userController");

router.get("/user_game/create", userController.userForm);
router.post("/user_game", userController.userPost);
router.get("/user_game/listUserGame", userController.userallPost);
router.get("/user_game/detailUserGame/:id", userController.useronePost);
router.get("/user_game/updateUserGame/:id", userController.userUpdate);
router.post("/user_game/updateUserGame", userController.userpostUpdate);
router.get("/user_game/deleteUserGame/:id", userController.userDelete);

module.exports = router;
