const router = require("express").Router();
const historyController = require("../controllers/historyController");

router.get("/user_game_history/create", historyController.createHistory);
router.post("/user_game_history", historyController.postHistory);
router.get("/user_game_history/listhistory", historyController.allHistory);
router.get(
  "/user_game_history/detailhistory/:id",
  historyController.oneHistory
);
router.get(
  "/user_game_history/updatehistory/:id",
  historyController.updateHistory
);
router.post(
  "/user_game_history/updatehistory",
  historyController.historypostUpdate
);
router.get(
  "/user_game_history/deletehistory/:id",
  historyController.deleteHistory
);
module.exports = router;
